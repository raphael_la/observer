﻿using Passagiere;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Observer
{
    public partial class Form2 : Form
    {
        Random r = new Random(DateTime.Now.Millisecond);
        Anzeige a;
        ObserverTyp typ;
        List<IPushGateObservable> observers = new List<IPushGateObservable>();

        public enum ObserverTyp
        {
            Pull, Push
        }

        public Form2(ObserverTyp typ )
        {
            InitializeComponent();
            this.typ = typ;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            a = new Anzeige();
            
            PassagierFactory factory = new PassagierFactory();
            for (int i = 0; i < 60; i++)
            {
                
                if (typ == ObserverTyp.Pull)
                {
                    PassagierPull p = null;
                    p = factory.GetPassagierPull(a, this.Width);
                    SetRandomPosition(p);
                    p.BringToFront();
                    this.Controls.Add(p);
                }
                else
                {
                    var p = factory.GetPassagierPush(this.Width);
                    SetRandomPosition(p);
                    p.BringToFront();
                    this.Controls.Add(p);
                    this.observers.Add(p);
                }
                
            }
            //ThreadPool.QueueUserWorkItem(new WaitCallback(RefreshForm));
            label1.SendToBack();
            label2.SendToBack();
        }

        private void RefreshForm(object state)
        {
            while (true)
            {
                this.Refresh();
                Thread.Sleep(20);
            }
        }


        private void SetRandomPosition(PassagierPull p)
        {
            p.Left = this.Width / 2 + r.Next(1, 200) - 100;
            p.Top = this.Height / 2 + r.Next(1, 200) - 100;
        }

        private void SetRandomPosition(PassagierPush p)
        {
            p.Left = this.Width / 2 + r.Next(1, 200) - 100;
            p.Top = this.Height / 2 + r.Next(1, 200) - 100;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            a.Gate = "A";
            label3.Text = "Gate: A";
            NotifyObservers();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a.Gate = "B";
            label3.Text = "Gate: B";
            NotifyObservers();
        }

        void NotifyObservers()
        {
            List<string> startTime = new List<string>();
            foreach(var o in observers)
            {
                o.Notify(label3.Text.Last().ToString());
            }
        }
    }
}
