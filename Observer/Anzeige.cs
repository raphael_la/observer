﻿using Passagiere;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class Anzeige : IPullGateObservable
    {
        public string Gate { get; set; } = string.Empty;

        public string CurrentGate()
        {
            return Gate;
        }
    }
}
