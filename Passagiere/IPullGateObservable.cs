﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Passagiere
{
    public interface IPullGateObservable
    {
        string CurrentGate();
    }
}
