﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Passagiere
{
    public class PassagierPull : Label
    {
        enum Direction
        {
            Left, Right
        }
        IPullGateObservable observable;
        Random r = new Random(Guid.NewGuid().GetHashCode());
        int formWidth;
        string gate = string.Empty;

        public PassagierPull(Color color, IPullGateObservable observable, int formWidth)
        {
            this.Height = this.Width = 10;
            this.BackColor = color;
            this.observable = observable;
            Thread t = new Thread(new ThreadStart(observeAnzeige));
            t.Start();
            this.formWidth = formWidth;
        }

        private void observeAnzeige()
        {

            var waitTime = r.Next(2000, 10000);
            
            while (true)
            {
                Thread.Sleep(waitTime);
                if (!observable.CurrentGate().Equals(gate))
                {
                    Move();
                }
                gate = observable.CurrentGate();
            }
        }

        int interval = 3;
        int currentPosition = 0;
        private new void Move()
        {

            var stopPositionLeft = r.Next(20, 300);
            var stopPositionRight = formWidth - r.Next(40, 300);


            int wait = 200;// r.Next(200, 500);

            while (true)
            {
                var direction = observable.CurrentGate().Equals("A") ? Direction.Left : Direction.Right;
                if (direction == Direction.Left)
                {
                    interval = -13;
                    this.Invoke(new MethodInvoker(this.MoveByInterval));

                    if (currentPosition < stopPositionLeft)
                    {
                        break;
                    }
                }
                else
                {
                    interval = 13;
                    this.Invoke(new MethodInvoker(this.MoveByInterval));

                    if (currentPosition > stopPositionRight)
                    {
                        break;
                    }
                }
                Thread.Sleep(wait);
            }
        }

        private void MoveByInterval()
        {
            this.Left += interval;
            currentPosition = this.Left;
        }
    }
}
