﻿using Passagiere.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Passagiere
{
    public class PassagierFactory
    {
        Random r = new Random(DateTime.Now.Millisecond);
        Dictionary<int, Color> images = new Dictionary<int, Color>
        {
            {1, Color.Red },
            {2, Color.Blue },
            {3, Color.Green },
            {4, Color.Orange },
            {5, Color.Brown },
            {6, Color.Yellow },
            {7, Color.Pink },
            {8, Color.Purple },
            {9, Color.Black }
        };

        public PassagierPull GetPassagierPull(IPullGateObservable observable, int formWidth)
        {
            PassagierPull p = new PassagierPull(images[r.Next(1, 10)], observable, formWidth);
            return p;
        }

        public PassagierPush GetPassagierPush(int formWidth)
        {
            PassagierPush p = new PassagierPush(images[r.Next(1, 10)], formWidth);
            return p;
        }
    }
}
