﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Passagiere
{
    public class PassagierPush : Label, IPushGateObservable
    {
        enum Direction
        {
            Left, Right
        }
        Random r = new Random(Guid.NewGuid().GetHashCode());
        int formWidth;
        string gate = string.Empty;

        public PassagierPush(Color color, int formWidth)
        {
            this.Height = this.Width = 10;
            this.BackColor = color;
            this.formWidth = formWidth;
        }


        int interval = 3;
        int currentPosition = 0;
        private new void Move()
        {
            var stopPositionLeft = r.Next(20, 300);
            var stopPositionRight = formWidth - r.Next(40, 300);


            int wait = 200;// r.Next(200, 500);

            while (true)
            {
                var direction = gate.Equals("A") ? Direction.Left : Direction.Right;
                if (direction == Direction.Left)
                {
                    interval = -13;
                    this.Invoke(new MethodInvoker(this.MoveByInterval));

                    if (currentPosition < stopPositionLeft)
                    {
                        break;
                    }
                }
                else
                {
                    interval = 13;
                    this.Invoke(new MethodInvoker(this.MoveByInterval));

                    if (currentPosition > stopPositionRight)
                    {
                        break;
                    }
                }
                Thread.Sleep(wait);
            }
            isMoving = false;
        }

        private void MoveByInterval()
        {
            this.Left += interval;
            currentPosition = this.Left;
        }

        bool isMoving = false;
        public void Notify(string gate)
        {
            this.gate = gate;
            if (isMoving)
                return;
            isMoving = true;
            Thread t = new Thread(new ThreadStart(Move)); //ThreadPool hat hier Probleme!
            t.Start();
        }

        
    }
}
